#!/usr/bin/env python
# -- coding: utf-8 --
import random
import time
from subprocess import call
lista = []
lista_final = []
inicio = 0
vecino_vivo = 0

print("**BIENVENIDOS AL JUEGO DE LA VIDA DE CONWAY**")
# Se solicita el tamaño de la matriz al usuario
tamano = int(input("Ingrese la dimensión que tendrá la matriz: "))


def condiciones(i, inicio, tamano, lista, vecino_vivo):
    while (i < (tamano * tamano)):
        # condiciones para el lado izquierdo
        if (i == 0) or (i == inicio):
            inicio = inicio + tamano
            if (lista[i] == "[X]"):
                # Se analiza el lado de arriba
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado de arriba a la derecha
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado derecho
                if(lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado de abajo en el medio
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado de abajo a la derecha
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se ven las condiciones de los muertos
            if(lista[i] == "[-]"):
                # Se ve la posiicion de arriba
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # La posicion de arriba a la derecha
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # La posicion de la derecha
                if(lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                # La posicion de abajo
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # La posicion de abajo a la derecha
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
        # condiciones para el lado derecho
        elif (((i + 1) % tamano) == 0):
            if (lista[i] == "[X]"):
                # Se ven arriba la matriz
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se ve abajo la matriz
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado de arriba izquierdo
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado izquierdo
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se analiza el lado de abajo izquierdo
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se ve los muertos
            if(lista[i] == "[-]"):
                # Se evalua la posicion de arriba
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua la posicion de abajo
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua la posicion de arriiba izquierda
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado izquierdo
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado de abajo izquierdo
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
        # condiciones para las que estan al medio
        else:
            if (lista[i] == "[X]"):
                # Se evalua el lado de arriba
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado de arriba derecho
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado derecho
                if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado de abajo
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado de abajo derecho
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado de abajo izquierdo
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado izquierdo
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                # Se evalua el lado izquierdo de abajo
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                    # Se analiza la celula muerta
            if (lista[i] == "[-]"):
                # La posicion de arriba de la matriz
                if(((i - tamano) >= 0) and (lista[i - tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion de arriba a la derecha
                if(((i - tamano + 1) > 0) and (lista[i - tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion derecha
                if((i + 1) < (tamano * tamano)) and (lista[i + 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion de abajo
                if(((i + tamano) < ((tamano * tamano))) and (lista[i + tamano] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion de abajo a la derecha
                if(((i + tamano + 1) < (tamano * tamano)) and (lista[i + tamano + 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion de arriba a la izquierda
                if((i - tamano - 1) >= 0) and (lista[i - tamano - 1] == "[X]"):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion de la izquierda
                if(((i - 1) >= 0) and (lista[i - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
                    # La posicion de abajo a la izquierda
                if(((i + tamano - 1) < (tamano * tamano)) and (lista[i + tamano - 1] == "[X]")):
                    vecino_vivo = vecino_vivo + 1
        reglas(i, vecino_vivo, lista, lista_final)
        i = i + 1
        vecino_vivo = 0
    return lista_final


# Se definen las reglas de supervivencia
def reglas(i, vecino_vivo, lista, lista_final):
    # De esta manera la celula sobrevive
    if((lista[i] == "[X]") and ((vecino_vivo == 2) or (vecino_vivo == 3))):
        lista_final.append("[X]")
        # La celula muere por sobrepoblacion
    elif((lista[i] == "[X]") and (vecino_vivo >= 4)):
        lista_final.append("[-]")
        # La celula muere por soledad
    elif((lista[i] == "[X]") and (vecino_vivo <= 1)):
        lista_final.append("[-]")
        # La celula volvera a vivir
    elif((lista[i] == "[-]") and (vecino_vivo == 3)):
        lista_final.append("[X]")
    else:
        lista_final.append("[-]")
    return lista_final


# Se termina el juego cuando las celulas muertas completen la matriz
def continuidad(tamano, validacion_juego, lista_final):
    stop = 1
    validacion_juego = 0
    for i in range(tamano * tamano):
        if(lista_final[i] == "[-]"):
            validacion_juego = validacion_juego + 1
    if(validacion_juego == (tamano * tamano)):
        stop = 0
# Se imprimen de la segunda generacion las celulas muertas y vivas
    celulas_muertas = validacion_juego
    celulas_vivas = (tamano * tamano) - validacion_juego
    print("Celulas vivas:", celulas_vivas)
    print("Celulas muertas:", celulas_muertas)
    return stop


# Se genera la funcion que imprime la matriz
def imprimir(tamano, lista_final):
    for j in range(tamano * tamano):
        print(lista_final[j], end="")
        if ((j + 1) % tamano == 0):
            print("\n")
# Se genera la matriz inicial y su forma por medio de listas


for i in range(tamano * tamano):
    estado = random.randint(0, 1)
    if (estado == 0):
        lista.append("[X]")
    else:
        lista.append("[-]")
    print(lista[i], end="")
    if ((i + 1) % tamano == 0):
        print("\n")
stop = 1
# Se efectua el main donde se reunen las funciones
while (stop != 0):
    validacion_juego = 1
    i = 0
    condiciones(i, inicio, tamano, lista, vecino_vivo)
    # De esta manera el tiempo es evaluado y se limpia la terminal
    time.sleep(1.4)
    call('clear')
    imprimir(tamano, lista_final)
    stop = continuidad(tamano, validacion_juego, lista_final)
    i = 0
    for i in range(tamano * tamano):
        lista[i] = lista_final[i]
    lista_final = []
